import static org.junit.Assert.*;

import org.junit.Test;

public class TestCase {
	
	
	@Test
	public void test() {
		EvenNumbers x = new EvenNumbers();
		boolean actualOutput = x.isEven(2);
		assertEquals(true, actualOutput);
	}

}
